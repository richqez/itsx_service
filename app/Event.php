<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;

class Event extends Model{

	protected $table = 'event' ;

	protected $fillable = ['event_id', 'event_name','event_description','event_status','event_level','event_user','event_lat','event_lng'];

	
}

 ?>