<?php 

namespace App;
 
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract{

	use Authenticatable, CanResetPassword;

	protected $table = 'users' ;

	protected $primaryKey = 'user_id';

	protected $fillable = ['user_type','user_phone','password','email'];

	protected $hidden = ['password', 'remember_token'];

	public function setPasswordAttribute($value)
	{
	    $this->attributes['password'] = \Hash::make($value);
	}

}

 ?>