<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;

class Hospital extends Model{

	protected $table = 'hospital' ;

	protected $primaryKey = 'hospital_id';

	protected $fillable = ['hospital_id', 'hospital_name','hospital_address','hospital_lat','hospital_lng'];

	
}

 ?>