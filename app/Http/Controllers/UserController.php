<?php 

namespace App\Http\Controllers;
 
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller {


	public function find_all(){
		$users = User::all();
		return response()->json($users); 
	}


	public function getUser($id){
		$user = User::find($id);
		return response()->json($user);

	}

	public function createUser(Request $request){
		$user = User::create($request->all());
		return response()->json($user);
	}

	public function updateUser(Request $request,$id){
		$user = User::find($id);
		$user->user_phone = $request->input('user_phone');
		$user->user_password = $request->input('user_password');
		$user->save();
		return response()->json($user);
	}

	public function deleteUser($id){
		$user = User::where('user_id',$id)->delete();
		return response()->json('deleted');
	}



}












 ?>