<?php 
	
namespace App\Http\Controllers;
 
use DB;
use App\Hospital;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HospitalController extends Controller {


	public function find_all(){
		$hospitals = Hospital::all();
		 $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
            );
		return response()->json($hospitals,200,$header,JSON_UNESCAPED_UNICODE); 
	}



	public function find_all_m(){
		$hospitals = Hospital::all();

		 

		 $markers = [] ;

		 foreach ($hospitals as $key => $value) {
		 	array_push($markers,
		 					array(
		 						"id"=>$value->hospital_id,
		 						"latitude"=>$value->hospital_lat,
		 						"longitude"=>$value->hospital_lng,
		 						"icon"=>"blue_marker.png",
		 						"options"=>array(
		 							"animation"=>1,
		 							"labelContent"=>$value->hospital_name,
		 							"labelAnchor"=>"22 0",
		 							"labelClass"=>"marker-labels"
		 							)

		 					));
		 }


		 $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
            );
		return response()->json($markers,200,$header,JSON_UNESCAPED_UNICODE); 
	}




	public function getHospital($id){
		$hospital = Hospital::find($id);
		return response()->json($hospital);

	}

	public function createHospital(Request $request){
		$hospital = Hospital::create($request->all());
		return response()->json($hospital);
	}


	public function nearByHospital($lat,$lng){


		$hospitals = DB::select('SELECT `hospital_id`,`hospital_name`,`hospital_lat`,`hospital_lng`, 
			( 6371 * acos( cos( radians(?) ) * cos( radians( `hospital_lat` ) ) * 
			cos( radians( `hospital_lng` ) - radians(?) ) + sin( radians(?) ) *
			sin( radians( `hospital_lat` ) ) ) ) 
			AS distance FROM hospital HAVING distance < 25 ORDER BY distance LIMIT 0 , 20'
			,[$lat,$lng,$lat]);


		 $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
            );

		return response()->json($hospitals,200,$header,JSON_UNESCAPED_UNICODE);




	}

	public function updateHospital(Request $request,$id){
		$hospital = Hospital::find($id);
		$hospital->hospital_name = $request->input('hospital_name');
		$hospital->hospital_address = $request->input('hospital_address');
		$hospital->hospital_lat = $request->input('hospital_lat');
		$hospital->hospital_lng = $request->input('hospital_lng');
		$hospital->save();

		return response()->json($hospital);
	}

	public function deleteHospital($id){
		$hospital = Hospital::find($id)->delete();
		return response()->json('deleted');
	}



}


 ?>