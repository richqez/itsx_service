<?php 

namespace App\Http\Controllers;
use DB ;
use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller {


	public function find_all(){
		$events = Event::all();
		return response()->json($events);
	}

	public function find_appoved(){
		$events = Event::where('event_status',"A")->get();
		return response()->json($events);
	}

	public function find_unappoved(){

		$events = Event::where('event_status',"U")->get();
		return response()->json($events);

	}



	public function createEvent(Request $request){
		$event = Event::create($request->all());
		return response()->json($event);
	}

	public function nearByEvent($lat,$lng){
		$events = DB::select('SELECT `event_id`,`event_name`,`event_lat`,`event_lat`,`event_user`,`event_level`,`event_description`
			( 6371 * acos( cos( radians(?) ) * cos( radians( `event_lat` ) ) * 
			cos( radians( `event_lng` ) - radians(?) ) + sin( radians(?) ) *
			sin( radians( `event_lat` ) ) ) ) 
			AS distance FROM event HAVING distance < 25 ORDER BY distance LIMIT 0 , 20'
			,[$lat,$lng,$lat]);

		return response()->json($evens);
	}


	public function updateEvent(Request $request,$id){
		$event = Event::find($id);
		$event->event_name = $request->input('event_name');
		$event->event_description = $request->input('event_description');
		$event->event_status = $request->input('event_status');
		$event->event_level = $request->input('event_level');
		$event->event_user = $request->input('event_user');
		$event->event_lat = $request->input('event_lat');
		$event->event_lng = $request->input('event_lng');
		$event->save();

		return response()->json($event);
	}

	public function deleteEvent($id){
		$event = Event::find($id)->delete();
		return response()->json('deleted');
	}

}

 ?>