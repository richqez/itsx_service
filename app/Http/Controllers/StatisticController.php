<?php 

namespace App\Http\Controllers;
use DB ;
use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StatisticController extends Controller {


	public function dataStatistic(){
		$hospital = DB::select('select count(*) as hospital from hospital ');
		$user = DB::select('select count(*) as user from users');
		
		$event = DB::select('select count(*) as event_all from event');
		$event_u = DB::select("select count(*) as event_u from event where event_status = 'u' ");
		$event_a = DB::select("select count(*) as event_a from event where event_status = 'a' ");

		$collectEvent = array("all" => $event[0]->event_all,
							  "u" => $event_u[0]->event_u,
							  "a" => $event_a[0]->event_a);



		return response()->json(array("hospital" => $hospital[0]->hospital ,"user"=> $user[0]->user,"event"=>$collectEvent));
	}	


	public function userStatistic(){

		$data = DB::select('SELECT ( SELECT COUNT(*) FROM `users` WHERE MONTH(`created_at`) = MONTH(NOW()-INTERVAL 3 MONTH) ) as users_3, ( SELECT COUNT(*) FROM `users` WHERE MONTH(`created_at`) = MONTH(NOW()-INTERVAL 2 MONTH) ) as users_2, ( SELECT COUNT(*) FROM `users` WHERE MONTH(`created_at`) = MONTH(NOW()-INTERVAL 1 MONTH) ) as users_1, ( SELECT COUNT(*) FROM `users` WHERE MONTH(`created_at`) = MONTH(NOW()) )as users_0, ( SELECT MONTHNAME(NOW()-INTERVAL 3 MONTH) ) as musers_3, ( SELECT MONTHNAME(NOW()-INTERVAL 2 MONTH) ) as musers_2, ( SELECT MONTHNAME(NOW()-INTERVAL 1 MONTH) ) as musers_1, ( SELECT MONTHNAME(NOW()) ) as musers_0')[0];

		return response()->json(
			array("data"=>array(array($data->users_3,$data->users_2,$data->users_1,$data->users_0)),
				  "labels"=>array($data->musers_3,$data->musers_2,$data->musers_1,$data->musers_0)));

	}


	public function eventStatistic(){

		$data = DB::select('SELECT ( SELECT COUNT(*) FROM `event` WHERE MONTH(`created_at`) = MONTH(NOW()-INTERVAL 3 MONTH) ) as event_3, ( SELECT COUNT(*) FROM `event` WHERE MONTH(`created_at`) = MONTH(NOW()-INTERVAL 2 MONTH) ) as event_2, ( SELECT COUNT(*) FROM `event` WHERE MONTH(`created_at`) = MONTH(NOW()-INTERVAL 1 MONTH) ) as event_1, ( SELECT COUNT(*) FROM `event` WHERE MONTH(`created_at`) = MONTH(NOW()) )as event_0, ( SELECT MONTHNAME(NOW()-INTERVAL 3 MONTH) ) as mevent_3, ( SELECT MONTHNAME(NOW()-INTERVAL 2 MONTH) ) as mevent_2, ( SELECT MONTHNAME(NOW()-INTERVAL 1 MONTH) ) as mevent_1, ( SELECT MONTHNAME(NOW()) ) as mevent_0')[0];

		return response()->json(
			array("data"=>array(array($data->event_3,$data->event_2,$data->event_1,$data->event_0)),
				  "labels"=>array($data->mevent_3,$data->mevent_2,$data->mevent_1,$data->mevent_0)
				));
	}


	public function systeminfo(){

		echo phpinfo();

	}

}

 ?>