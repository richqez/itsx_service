<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('index');
});


/*
|--------------------------
|
|--------------------------	 
*/


$app->group(['middleware' => 'cors'],function($app){

	$app->post('auth/login','App\Http\Controllers\AuthController@authenticate');
	$app->post('register','App\Http\Controllers\UserController@createUser');
	$app->get('api/hospitals','App\Http\Controllers\HospitalController@find_all_m');
	$app->get('api/hospitaltest','App\Http\Controllers\HospitalController@find_all');
	$app->get('api/hospital/{lat}/{lng}','App\Http\Controllers\HospitalController@nearByHospital');


	$app->get('api/events','App\Http\Controllers\EventController@find_all');
	$app->post('api/event','App\Http\Controllers\EventController@createEvent');

	$app->get('api/events/a','App\Http\Controllers\EventController@find_appoved');
	$app->get('api/events/u','App\Http\Controllers\EventController@find_unappoved');



	$app->get('api/statistic/all','App\Http\Controllers\StatisticController@dataStatistic');
	$app->get('api/statistic/user','App\Http\Controllers\StatisticController@userStatistic');
	$app->get('api/statistic/event','App\Http\Controllers\StatisticController@eventStatistic');
	$app->get('api/statistic/systeminfo','App\Http\Controllers\StatisticController@systeminfo');

});


/*
|--------------------------
| 
|--------------------------	 
*/
$app->group(['middleware' => 'jwt.auth'],function($app){

	$app->get('api/user','App\Http\Controllers\UserController@find_all');
	$app->get('api/user/{id}','App\Http\Controllers\UserController@getUser');
	$app->put('api/user/{id}','App\Http\Controllers\UserController@updateUser');
	$app->delete('api/user/{id}','App\Http\Controllers\UserController@deleteUser');


	$app->get('api/hospital','App\Http\Controllers\HospitalController@find_all');
	$app->get('api/hospital/{id}','App\Http\Controllers\HospitalController@getHospital');
	$app->post('api/hospital','App\Http\Controllers\HospitalController@createHospital');
	$app->put('api/hospital/{id}','App\Http\Controllers\HospitalController@updateHospital');
	$app->delete('api/hospital/{id}','App\Http\Controllers\HospitalController@deleteHospital');
	

});







