-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 30, 2015 at 06:58 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `incident_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL COMMENT 'เลขที่อุบัติเหตุ',
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `event_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `event_status` tinyint(1) NOT NULL,
  `event_date` date NOT NULL,
  `event_level` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ระดับความรุนแรง',
  `event_user` int(11) NOT NULL,
  `event_lat` float NOT NULL COMMENT 'ลัตติจูดที่เกิดเหตุ',
  `event_lng` float NOT NULL COMMENT 'ลองติจูดที่เกิดเหตุ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

CREATE TABLE `hospital` (
  `hospital_id` int(11) NOT NULL,
  `hospital_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hospital_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ที่อยู่',
  `hospital_lat` float(10,6) NOT NULL COMMENT 'ลัตติจูด',
  `hospital_lng` float(10,6) NOT NULL COMMENT 'ลองติจูด',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hospital`
--

INSERT INTO `hospital` (`hospital_id`, `hospital_name`, `hospital_address`, `hospital_lat`, `hospital_lng`, `updated_at`, `created_at`) VALUES
(18, 'Siam University', '', 13.910376, 100.534203, '2015-11-30 16:52:17', '2015-11-30 16:41:52'),
(24, 'Bee Hive Lifestyle Mall', '', 13.910376, 100.534203, '2015-11-30 16:52:11', '2015-11-30 16:52:11'),
(25, 'เมืองทองธานี สัตวแพทย์', '', 13.911730, 100.536362, '2015-11-30 16:53:47', '2015-11-30 16:53:47'),
(26, 'สถาบันเทคโนโลยีแห่งเอเซีย', '', 14.080806, 100.601746, '2015-11-30 16:54:43', '2015-11-30 16:54:43'),
(27, 'นครสวรรค์', '', 15.622008, 99.399521, '2015-11-30 17:33:45', '2015-11-30 17:33:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL COMMENT 'ลำดับ',
  `user_type` int(11) NOT NULL COMMENT 'ประเภทยูสเซอร์',
  `user_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` binary(60) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_type`, `user_phone`, `password`, `updated_at`, `created_at`, `remember_token`, `email`) VALUES
(24, 2, '0865476328', 0x24327924313024694c706851624a38444a5038442f7a2e47366779364f35515453727633494949755a6636627954505745624f706c6a6b727a465a32, '2015-11-29 15:40:58', '2015-11-29 15:40:58', '', 'aaaa@gmail.com'),
(29, 2, '086-5476328', 0x243279243130246e6e46472e394f73372f5037516252464752454b4c2e3543486b487438716f362e4e5a7148376637695a633854784d344f5433524f, '2015-11-30 07:58:51', '2015-11-30 07:58:51', '', 'a@a.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `user_type_id` int(14) NOT NULL COMMENT 'ไอดี',
  `user_type_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อประเภทยูสเซอร์'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`user_type_id`, `user_type_name`) VALUES
(1, 'Nomal User'),
(2, 'Super User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `hospital`
--
ALTER TABLE `hospital`
  ADD PRIMARY KEY (`hospital_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `user_type` (`user_type`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'เลขที่อุบัติเหตุ';
--
-- AUTO_INCREMENT for table `hospital`
--
ALTER TABLE `hospital`
  MODIFY `hospital_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ลำดับ', AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `user_type_id` int(14) NOT NULL AUTO_INCREMENT COMMENT 'ไอดี', AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_type`) REFERENCES `user_type` (`user_type_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
